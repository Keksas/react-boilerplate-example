import { API_USER, API_PASS } from './constant';

const auth = btoa(`${API_USER}:${API_PASS}`);

const prepareUri = (uri, params) => {
  if (typeof params.query !== 'undefined') {
    const query = Object.keys(params.query)
      .filter(key => typeof params.query[key] !== 'undefined')
      .map(key => `${encodeURI(key)}=${encodeURI(params.query[key])}`)
      .join('&');
    return `${uri}?${query}`;
  }
  return `${uri}`;
};

const prepareRequest = (params) => {
  const headers = new Headers({
    Authorization: `Basic ${auth}`
  });

  const nextParams = Object.assign({}, params, {
    credentials: 'include'
  });

  if (typeof params.body !== 'undefined') {
    headers.append('Content-Type', 'application/json');
    nextParams.body = JSON.stringify(nextParams.body);
  }

  nextParams.headers = headers;

  return nextParams;
};

const responseValidateHttp = (res) => {
  if (!res.ok) {
    throw new Error(res.statusText);
  }
  return res;
};

const responseParse = res => res.json()
  .catch(() => {
    throw new Error('Failed to parse response');
  });

const responseData = (res) => {
  if (!res || !res.status) {
    throw new Error('Malformed response');
  }

  if (res.status === 'error') {
    throw new Error(res.message || 'API Error');
  }

  return res.data;
};

export default (uri, params = {}) => fetch(prepareUri(uri, params), prepareRequest(params))
  .then(responseValidateHttp)
  .then(responseParse)
  .then(responseData);
