import React, { Component } from 'react';
import fetch from '../fetch';
import Layout from './Layout';
import Counter from './Counter';
import Button from './Button';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      res: null
    };
  }

  ping = () => {
    this.setState({ res: null });
    fetch('/ping')
      .then((res) => {
        this.setState({ res });
      });
  }

  render() {
    return (
      <Layout>
        <Counter />
        <Button onClick={this.ping} label="ping" />
        <pre>{this.state.res/* eslint react/destructuring-assignment:off */}</pre>
      </Layout>
    );
  }
}
