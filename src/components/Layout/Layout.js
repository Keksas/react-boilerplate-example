import React from 'react';
import PropTypes from 'prop-types';

import './Layout.scss';

const propTypes = {
  children: PropTypes.node
};

const defaultProps = {
  children: null
};

class Layout extends React.Component {
  render() {
    const { children } = this.props;
    return (
      <div className="Layout">
        <h1>Hello, world!</h1>
        {children}
      </div>
    );
  }
}

Layout.propTypes = propTypes;
Layout.defaultProps = defaultProps;
export default Layout;
