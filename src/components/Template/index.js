import { hot } from 'react-hot-loader';
import Component from './Template';

export default hot(module)(() => <Component />);
