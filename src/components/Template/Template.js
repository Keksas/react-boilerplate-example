import React from 'react';
import PropTypes from 'prop-types';

import './Template.scss';

export default class Template extends React.Component {
  static defaultProps = {
    title: 'TEMAPLTE'
  };

  static propTypes = {
    title: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    //
  }

  componentWillUnmount() {
    //
  }

  render() {
    const { title } = this.props;
    return (
      <div>{title}</div>
    );
  }
}
