import React from 'react';
import PropTypes from 'prop-types';

import './Button.scss';

const propTypes = {
  onClick: PropTypes.func.isRequired,
  label: PropTypes.string
};

const defaultProps = {
  label: 'Empty Button'
};

class Button extends React.Component {
  render() {
    const { onClick, label } = this.props;
    return (
      <button type="button" onClick={onClick}>
        {label}
      </button>
    );
  }
}

Button.propTypes = propTypes;
Button.defaultProps = defaultProps;
export default Button;
