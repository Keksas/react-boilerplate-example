import React from 'react';
import ReactDOM from 'react-dom';
import App from './hot';

ReactDOM.render(<App />, document.getElementById('root'));
