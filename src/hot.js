import React from 'react';
import { hot } from 'react-hot-loader';
import App from './components/App';

/**
 * Example wrapper for controling hot module reloading
 * @returns {node} App component
 */
export default hot(module)(() => (
  <App />
));
